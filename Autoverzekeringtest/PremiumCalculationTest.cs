using System;
using Xunit;

using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace Autoverzekeringtest
{
    public class PremiumCalculationTest
    {
        [Theory]
        [InlineData(80, 7000, 2004, 31, "12-03-2006", 2363, 2, InsuranceCoverage.WA, PremiumCalculation.PaymentPeriod.MONTH, 2.01)]
        [InlineData(80, 7000, 2004, 23, "12-03-2006", 2363, 2, InsuranceCoverage.WA, PremiumCalculation.PaymentPeriod.MONTH, 2.01)]
        [InlineData(80, 7000, 2004, 19, "16-06-2016", 2363, 2, InsuranceCoverage.WA, PremiumCalculation.PaymentPeriod.MONTH, 2.31)]
        [InlineData(80, 7000, 2004, 19, "16-06-2016", 2363, 18, InsuranceCoverage.WA, PremiumCalculation.PaymentPeriod.MONTH, 0.81)]
        [InlineData(80, 7000, 2004, 19, "16-05-2016", 2363, 5, InsuranceCoverage.WA, PremiumCalculation.PaymentPeriod.MONTH, 2.31)]
        [InlineData(80, 7000, 2004, 19, "16-05-2016", 0600, 5, InsuranceCoverage.WA, PremiumCalculation.PaymentPeriod.MONTH, 2.25)]
        [InlineData(80, 7000, 2004, 19, "16-05-2016", 9999, 5, InsuranceCoverage.WA, PremiumCalculation.PaymentPeriod.MONTH, 2.2)]
        [InlineData(80, 7000, 2004, 19, "16-05-2016", 0600, 5, InsuranceCoverage.WA_PLUS, PremiumCalculation.PaymentPeriod.MONTH, 2.7)]
        [InlineData(80, 7000, 2004, 19, "16-05-2016", 9999, 5, InsuranceCoverage.ALL_RISK, PremiumCalculation.PaymentPeriod.MONTH, 4.41)]
        [InlineData(80, 7000, 2004, 19, "16-05-2016", 9999, 5, InsuranceCoverage.ALL_RISK, PremiumCalculation.PaymentPeriod.YEAR, 51.61)]
        [InlineData(80, 7000, 2004, 56, "16-05-2016", 9999, 5, InsuranceCoverage.ALL_RISK, PremiumCalculation.PaymentPeriod.YEAR, 44.88)]
        [InlineData(80, 7000, 2021, 56, "16-05-2016", 9999, 5, InsuranceCoverage.ALL_RISK, PremiumCalculation.PaymentPeriod.YEAR, 54.63)]
        private void TestPremium(int power, int value, int constructionYear, int age, string driverLicenseStartDate, int postalCode, int noClaimYears, InsuranceCoverage coverage, PremiumCalculation.PaymentPeriod paymentPeriod, double expectedPremium)
        {
            Vehicle vehicle = new Vehicle(power, value, constructionYear);
            PolicyHolder policyHolder = new PolicyHolder(age, driverLicenseStartDate, postalCode, noClaimYears);

            PremiumCalculation premiumCalculation = new PremiumCalculation(vehicle, policyHolder, coverage);
            double actualPremium = premiumCalculation.PremiumPaymentAmount(paymentPeriod);

            Assert.Equal(expectedPremium, actualPremium);
        }
    }
}
